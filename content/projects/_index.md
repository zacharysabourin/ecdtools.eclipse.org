---
title: "Projects"
headline: "Projects"
date: 2020-09-30T14:53:00+02:00
hide_page_title: true
hide_sidebar: true
jumbotron_bg_class: "featured-jumbotron-sub-pages header-projects-bg-img"
container: "container-fluid"
main_content_class: "col-sm-18 col-sm-offset-3"
---

{{< grid/div class="row featured-section featured-section-prefix text-center featured-content-white-bg" isMarkdown="false" >}}
    {{< grid/div class="container" isMarkdown="false" >}}
        {{< grid/div class="row" isMarkdown="false" >}}
            {{< grid/div class="col-md-14 col-md-offset-5 col-sm-18 col-sm-offset-3" isMarkdown="false" >}}
                <p class="small">PROJECTS</p>
                <p class="powered-by-description">The Eclipse Cloud DevTools Working Group serves to
                advance, promote, and drive community participation in a
                broad portfolio of Eclipse projects.</p>
                <p><a class="btn btn-primary" href="https://projects.eclipse.org/projects/ecd">Learn More</a></p>
            {{</ grid/div >}}
        {{</ grid/div >}}
    {{</ grid/div >}}
{{</ grid/div >}}

{{< eclipsefdn_projects
    templateId="tpl-projects-item"
    url="https://projects.eclipse.org/api/projects?working_group=cloud-development-tools"
    classes="margin-top-30"
    display_categories="true"
    categories="/data/featured-projects-categories.json"
>}}
